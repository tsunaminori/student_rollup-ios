//
//  HomePageViewController.swift
//  DOAN_iOS_
//
//  Created by Nguyen Dang Thien Tam on 10/25/18.
//  Copyright © 2018 Nguyen Dang Thien Tam. All rights reserved.
//

import UIKit

class HomePageViewController: UIViewController {

    @IBOutlet weak var menuHome: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        self.menuHome.target = self.revealViewController()
        self.menuHome.action = Selector("revealToggle:")
        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
