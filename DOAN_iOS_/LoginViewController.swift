//
//  ViewController.swift
//  DOAN_iOS_
//
//  Created by Nguyen Dang Thien Tam on 10/25/18.
//  Copyright © 2018 Nguyen Dang Thien Tam. All rights reserved.
//

import UIKit
import Firebase

class LoginViewController: UIViewController {
    
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var username: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        print("AA")
        let customColor = (UIColor(red: 0.33, green: 1, blue: 0.81, alpha: 1))
        buttonLogin.layer.borderColor = UIColor.white.cgColor
                buttonLogin.layer.borderWidth = 2
        buttonLogin.layer.cornerRadius = 6

        buttonLogin.layer.masksToBounds = false
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func clickLogin(_ sender: Any) {
        Auth.auth().signIn(withEmail : username.text!, password : password.text!) {
            (user, error) in
            if error != nil {
                print(error!)
            } else {
                print("THANHCONG")
                let sb = UIStoryboard(name: "Main", bundle: nil)
                let navigate = sb.instantiateViewController(withIdentifier: "DiemDanhDiHocViewController") as! DiemDanhDiHocViewController
                self.navigationController?.pushViewController(navigate, animated: true)
            }
        }
      
        
    }
    
}

