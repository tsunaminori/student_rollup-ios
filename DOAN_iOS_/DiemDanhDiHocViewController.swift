//
//  DiemDanhDiHocViewController.swift
//  DOAN_iOS_
//
//  Created by Nguyen Dang Thien Tam on 10/25/18.
//  Copyright © 2018 Nguyen Dang Thien Tam. All rights reserved.
//

import UIKit
import FirebaseStorage
import FirebaseDatabase
import FirebaseCore
import FirebaseAuth

class DiemDanhDiHocViewController: UIViewController {

    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var txtDateRollUp: UITextField!
    @IBOutlet weak var txtMaLop: UITextField!
    @IBOutlet weak var imageView: UIImageView!
    
    
    //Action
    @objc func tapDetected() {
        print("Imageview Clicked")
    }
    var ref: DatabaseReference!

    var imageTake = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        imageTake.delegate = self
     loadDateTime()
        let userID = Auth.auth().currentUser?.uid
        let singleTap = UITapGestureRecognizer(target: self, action: Selector("chooseLibrary"))
        imageView.isUserInteractionEnabled = true
        imageView.addGestureRecognizer(singleTap)
        
    }
    
    func loadDateTime(){
        let dateToday =  Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM-yyyy"
        var timeResult : String = ""
        let calendar = Calendar.current
        var dateResult = dateFormatter.string(from: dateToday)
        timeResult = "\(calendar.component(.hour, from: dateToday)):\(calendar.component(.minute, from: dateToday))"
        txtDateRollUp.text = dateResult
        txtTime.text = timeResult
    }
    @IBAction func takePhoto(_ sender: Any) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = true
        vc.delegate = self
        present(vc, animated: true)
    }
    
    @IBAction func subMit(_ sender: Any) {
        uploadImage()
    }
    
   
    @objc func chooseLibrary() {
        imageTake.sourceType = .photoLibrary
        imageTake.allowsEditing = true
        present(imageTake, animated: true, completion: nil)
        
    }
    func addDataToDatabase()  {
        
        ref = Database.database().reference()
        self.ref.child("DIEMDANHSINHVIEN_DIHOC").child("MACLENIN1_BIS2").child("Image").child("27-10-2018").child("20:10:20").setValue([["Url": "TAMNGUYEN"],["TEST":"TEST"]])

    }
    
    func  uploadImage()  {
        let storage  = Storage.storage(url:"gs://student-rollup.appspot.com")
        let storageRef = storage.reference()
        let data: Data = self.imageView.image!.pngData()!
        let metadata = StorageMetadata()
        metadata.contentType = "image/jpeg"
        let riversRef = storageRef.child("diemdanh/\(txtMaLop.text!)/\(txtDateRollUp.text!)/\(txtTime.text!)")
        
        let uploadTask = riversRef.putData(data, metadata: metadata) { metadata, error in
            
            if error != nil {
                print(error!)
            }
            print("In meta")
            print(metadata!)
            riversRef.downloadURL { (url, error) in
                if error != nil {
                    print(error!)
                } else {
                let profileImageUrl = url!.absoluteString
                print(profileImageUrl)
                print("IN URL")
                print(profileImageUrl)
                    self.addDataToDatabase()
                }
            }
        }
        print("tai:")
       
    }
    func uploadMedia(completion: @escaping (_ url: String?) -> Void) {
    }
    

}
extension DiemDanhDiHocViewController : UIImagePickerControllerDelegate , UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            imageView.contentMode = .scaleToFill
            imageView.image = image
        }
        dismiss(animated: true, completion: nil)
    
    
    }
    
}
